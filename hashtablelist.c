#include <glib-2.0/glib.h>
#include<stdio.h>
void print_array (gpointer value,gpointer user_data)
{
 	gchar *val=value;
  	g_print (" %s\n",val);
}
void print_element (gpointer key,gpointer value,gpointer user_data)
{
 	 if(key && value)
	{
  		gchar *str = key;
  		GPtrArray *valuelist=(GPtrArray *)value;
  		g_print ("table['%s'] = ", str);
		g_ptr_array_foreach (valuelist,print_array,NULL);	
		
	}
	else
	return;
}

gint main (gint    argc,  gchar **argv)
{	GHashTable *table;  
	table = g_hash_table_new (g_str_hash, g_str_equal);	
	printf("Welcome to Hash Table Function\n");
	/*GPtrArray *defaultvalue;
	g_ptr_array_add(defaultvalue,"default");
	g_hash_table_insert(table,"default",(gpointer)defaultvalue);
	g_ptr_array_free(defaultvalue,TRUE); */
 	printf("Menu\n");
   	printf("1. View Table\n 2.Add Key Value Pairs");
	int choice=1;
	while(scanf("%d",&choice))
	{
		if(choice==1)
		{
			g_hash_table_foreach (table, print_element, NULL);
		}
		else if(choice==2)
		{
			printf("Enter Key");
			gchar key[50] , value[50];
			scanf("%s",key);
			printf("Enter Value");
			scanf("%s",value);
			if(g_hash_table_lookup (table,(gpointer)key))
			{
				GPtrArray *valuelist=g_hash_table_lookup(table,key);
  				g_ptr_array_add (valuelist,(gpointer)value);
				g_hash_table_insert (table, (gpointer)key, (gpointer)valuelist);
   				g_ptr_array_free (valuelist,TRUE);
			}			
			else
			{	
				GPtrArray *valuelist=g_ptr_array_new ();
				g_ptr_array_add(valuelist,(gpointer)value);
   				g_hash_table_insert (table, (gpointer)key, (gpointer)valuelist);
				g_ptr_array_free (valuelist,TRUE);
			}
		}
		printf("1. View Table\n 2.Add Key Value Pairs");
	}  	
 	g_hash_table_unref (table);
}